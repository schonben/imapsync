#!/usr/bin/php
<?php
require_once "imapsync.class.php";

$settings = yaml_parse_file("settings.yaml");

imap_timeout(1, 600);
imap_timeout(2, 600);
imap_timeout(3, 600);
imap_timeout(4, 600);

$imap = new \ImapSync\Sync($settings['source'], $settings['destination'], $settings['verbose']);
foreach ($settings['mailboxes'] as $mailbox) {
    $duser = (!empty($mailbox['destination']) && !empty($mailbox['destination']['user'])) ? $mailbox['destination']['user'] : '';
    $dpass = (!empty($mailbox['destination']) && !empty($mailbox['destination']['pass'])) ? $mailbox['destination']['pass'] : '';

    $imap->openImap(
        $mailbox['source']['user'],
        $mailbox['source']['pass'],
        $duser,
        $dpass
    );
    $imap->list();
    $imap->subscribe();
    $imap->syncronize();
}
$imap->closeImap();
