<?php
namespace ImapSync;

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", "stderr");
class Sync
{
    protected $source = null;
    protected $sourceRef = "";
    protected $destination = null;
    protected $destinationRef = "";
    protected $folders = [];
    protected $verbose = 2;

    public function __construct($source, $destination, $verbose = 2)
    {
        $this->sourceRef = $source;
        $this->destinationRef = $destination;
        if (!empty($verbose)) {
            $this->verbose = $verbose;
        }
    }

    public function openImap($suser, $spass, $duser = "", $dpass = "")
    {
        $this->closeImap();

        if (empty($duser)) {
            $duser = $suser;
        }
        if (empty($dpass)) {
            $dpass = $spass;
        }

        $this->e("", 2);
        $this->e("Connecting to $suser");

        $this->source = imap_open($this->sourceRef, $suser, $spass);
        if ($this->source === false) {
            $this->showErrors("Source");
            exit();
        }

        $this->destination = imap_open($this->destinationRef, $duser, $dpass);
        if ($this->destination === false) {
            $this->showErrors("Destination");
            exit();
        }
    }

    public function closeImap()
    {
        $this->showErrors("Close");
        if ($this->source !== null) {
            imap_close($this->source);
            $this->source = null;
            $this->e("Closing Source", 3);
        }
        if ($this->destination !== null) {
            imap_close($this->destination);
            $this->destination = null;
            $this->e("Closing Destination", 3);
        }
    }

    public function list()
    {
        $this->folders = $this->getFolders($this->source, $this->sourceRef);
        $destFolders = $this->getFolders($this->destination, $this->destinationRef);

        foreach ($this->folders as $folder) {
            if (!in_array($folder, $destFolders)) {
                // Create the "mailbox".
                $this->e("Mailbox not found: $folder, creating..");
                if (imap_createmailbox($this->destination, imap_utf7_encode($this->destinationRef . $folder))) {
                    $this->e("$folder created successfully.");
                } else {
                    $this->e("Creation of $folder failed!", 1);
                }
            } else {
                $this->e("Folder: $folder found.", 3);
            }
        }
    }

    public function subscribe()
    {
        $sourceFolders = $this->getFolders($this->source, $this->sourceRef, true);
        $destFolders = $this->getFolders($this->destination, $this->destinationRef, true);
        foreach ($sourceFolders as $folder) {
            if (!in_array($folder, $destFolders)) {
                // Not subscribed.
                $this->e("Mailbox not subscribed: $folder, subscribing..");
                if (imap_subscribe($this->destination, imap_utf7_encode($this->destinationRef . $folder))) {
                    $this->e("$folder subscribed successfully.");
                } else {
                    $this->e("Subscription of $folder failed!", 1);
                }
            }
        }
        $this->showErrors("Subscribe");
    }

    public function syncronize()
    {
        foreach ($this->folders as $folder) {
            $this->e("Syncronize $folder.");
            $this->folderSync(imap_utf7_encode($folder));
        }
    }

    /**
     * Get all folders for the account.
     *
     * @param int $res Imap resource.
     * @param string $ref The imap reference string.
     * @return array The folders in the account.
     */
    protected function getFolders($res, $ref, $subscribed = false)
    {
        $list = $subscribed ? imap_lsub($res, $ref, "*") : imap_list($res, $ref, "*");
        $folders = [];
        if ($list !== false) {
            foreach ($list as $folder) {
                $name = substr($folder, strlen($ref));
                if ($name != "Trash" && $name != "Junk" && $name != "INBOX.Trash" && $name != "INBOX.Junk") {
                    $folders[] = imap_utf7_decode($name);
                }
            }
        }
        $this->showErrors("Folders");
        return $folders;
    }

    protected function folderSync($folder)
    {
        // Open the same mailbox on both.
        imap_reopen($this->source, $this->sourceRef . $folder);
        imap_reopen($this->destination, $this->destinationRef . $folder);
        imap_gc($this->source, IMAP_GC_ELT | IMAP_GC_ENV | IMAP_GC_TEXTS);

        $destCheck = imap_check($this->destination);
        $this->showErrors("Check Destination");
        $destIds = [];
        $this->e("Found " . $destCheck->Nmsgs . " messages in target.", 3);
        if ($destCheck->Nmsgs > 0) {
            $destMsgs = imap_fetch_overview($this->destination, '1:' . $destCheck->Nmsgs);
            $this->showErrors("Fetch Destination");
            if (!empty($destMsgs)) {
                foreach ($destMsgs as $msg) {
                    if (empty($msg->message_id)) {
                        $this->e("Empty message id");
                    } else {
                        $destIds[] = $msg->message_id;
                    }
                }
            }
        }
        $this->e("Target IDs in list: " . count($destIds), 4);

        $sourceCheck = imap_check($this->source);
        $this->showErrors("Check Source");
        $this->e($sourceCheck->Nmsgs . " messages in source.", 2);
        $total = ["found" => 0, "add" => 0, "empty" => 0];
        $toAdd = [];
        if ($sourceCheck->Nmsgs > 0) {
            $end = 0;
            do {
                $start = $end + 1;
                $end = $sourceCheck->Nmsgs;
                if ($end - $start > 99) {
                    $end = $start + 99;
                }
                $sourceMsgs = imap_fetch_overview($this->source, $start . ':' . $end);
                $this->showErrors("Fetch Overview");
                $this->e("Fetched " . count($sourceMsgs) . " source messages. (${start}:${end})", 4);
                if (!empty($sourceMsgs)) {
                    foreach ($sourceMsgs as $index => $msg) {
                        if (empty($msg->message_id)) {
                            $this->e("Empty message id, trying to create.");
                            $message_id = '<' . md5($msg->from . $msg->to . $msg->date . $msg->size) . '@mail.uplink.fi>';
                            $this->e($message_id, 3);
                            if (in_array($message_id, $destIds)) {
                                $this->e("message " . $message_id . " found.", 4);
                                $total["found"]++;
                            } else {
                                $msg->add_message_id = $message_id;
                                $toAdd[] = $msg;
                            }
                        } else {
                            $this->e("Checking " . $msg->subject . ".", 5);
                            if (in_array($msg->message_id, $destIds)) {
                                $this->e("message " . $msg->message_id . " found.", 4);
                                $total["found"]++;
                            } else {
                                $toAdd[] = $msg;
                            }
                        }
                    }
                }
            } while ($end < $sourceCheck->Nmsgs);
        }

        while (count($toAdd)) {
            if ($this->showErrors("Add mail loop")) {
                imap_reopen($this->source, $this->sourceRef . $folder);
            }
            $msg = array_pop($toAdd);
            switch ($this->addMail($msg, $folder)) {
                case 0:
                    $total["add"]++;
                    break;
                case 1:
                    // Fetch error
                    imap_reopen($this->source, $this->sourceRef . $folder);
                    // Return the value to the array.
                    array_unshift($toAdd, $msg);
                    break;
                case 2:
                    // Append error, ignore at the moment. No known fix.
                    break;
                case 3:
                    // Empty message
                    $total["empty"]++;
                    break;
            }
        };

        $this->e("Total Found: " . $total["found"]);
        $this->e("Total Added: " . $total["add"]);
        $this->e("Total Empty: " . $total["empty"], 3);
    }

    protected function addMail($message, $folder)
    {
        $elements = imap_mime_header_decode($message->subject);
        $subject = '';
        foreach ($elements as $element) {
            $subject .= $element->text;
        }

        $flags = '';
        if (!empty($message->seen)) {
            $flags .= '\Seen ';
        }
        if (!empty($message->answered)) {
            $flags .= '\Answered ';
        }
        if (!empty($message->flagged)) {
            $flags .= '\Flagged ';
        }
        if (!empty($message->draft)) {
            $flags .= '\Draft ';
        }
        if (!empty($message->deleted)) {
            $flags .= '\Deleted ';
        }
        $body = imap_fetchbody($this->source, $message->msgno, "");
        if ($this->showErrors("Message Fetch")) {
            return 1;
        }
        $body = str_replace("***UNCHECKED*** ", "", $body);
        if (!empty($message->add_message_id)) {
            $count = 0;
            $body = str_replace("\r\nSubject: ", "\r\nMessage-Id: $message->add_message_id\r\nSubject: ", $body, $count);
            if ($count !== 1) {
                $this->e("Message ID creation failed.");
                return 4;
            }
        }

        if (empty($body)) {
            $this->e('Empty Message.', 0);
            return 3;
        } else {
            if (imap_append($this->destination, $this->destinationRef . $folder, $body, trim($flags))) {
                $this->e('+ ' . $subject, 3);
                return 0;
            } else {
                $this->e("Message append failed!", 0);
                if ($this->showErrors("Message Append")) {
                    return 2;
                }
            }
        }
        return 9;
    }
    protected function parseHeaders($headers)
    {
        $head = [];
        foreach (explode("\n", $headers) as $row) {
            $parts = explode(":", $row, 2);
            if (count($parts) > 1) {
                $head[strtolower(trim($parts[0]))] = trim($parts[1]);
            }
        }
        return $head;
    }

    protected function e($text, $level = 2)
    {
        if ($this->verbose >= $level) {
            echo $text . "\n";
        }
    }

    protected function showErrors($message = "")
    {
        $errors = imap_errors();
        if (empty($errors)) {
            return false;
        }
        if (!empty($message)) {
            $this->e("Error in: " . $message, 0);
        }
        foreach ($errors as $error) {
            $this->e($error, 0);
        }
        return true;
    }
}
